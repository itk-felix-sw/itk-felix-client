#ifndef ITK_FELIX_BUS
#define ITK_FELIX_BUS

#include "itk-felix-client/itk-felix-bus-info.h"

#include <string>

class itk_felix_bus{

 public:
  
  itk_felix_bus();

  ~itk_felix_bus();

  void set_path(const std::string& path);

  void set_group(const std::string& group);

  void set_verbose(bool enable);

  void set_info(uint64_t fid, std::string filename, itk_felix_bus_info &info);

  itk_felix_bus_info get_info(uint64_t fid);

  std::vector<itk_felix_bus_info> get_infos();

  void scan();
  
 private:
  
  bool m_verbose;
  std::string m_path;
  std::string m_group;  
  std::map<uint64_t, itk_felix_bus_info> m_infos;
  
  void scan_dir(const std::string& path);
  void load_file(const std::string& path);

};


#endif
