#ifndef ITK_FELIX_CLIENT
#define ITK_FELIX_CLIENT

#include <cstdint>
#include <string>
#include <functional>
#include <vector>
#include <thread>
#include "netio/netio.h"
#include "felix-bus/bus.h"

typedef std::function<void ()> OnInitCallback;
typedef std::function<void (uint64_t fid, const uint8_t* data, size_t size, uint8_t status)> OnDataCallback;
typedef std::function<void (uint64_t fid)> OnConnectCallback;
typedef std::function<void (uint64_t fid)> OnDisconnectCallback;
typedef std::function<void ()> UserFunction;
typedef std::map<std::string, std::string> Properties;

class itk_felix_client{

 public:

  enum Cmd {
    UNKNOWN = 0,
    NOOP = 1,
    GET = 2,
    SET = 3,
    TRIGGER = 4,
    ECR_RESET = 5
  };

  enum Status {
    OK = 0,
    ERROR = 1,
    ERROR_NO_SUBSCRIPTION = 2,
    ERROR_NO_CONNECTION = 3,
    ERROR_NO_REPLY = 4,
    ERROR_INVALID_CMD = 5,
    ERROR_INVALID_ARGS = 6,
    ERROR_INVALID_REGISTER = 7,
    ERROR_NOT_AUTHORIZED = 8
  };

  struct Reply {
    uint64_t ctrl_fid;
    Status status;
    uint64_t value;
    std::string message;
  };

  struct Config {
    OnInitCallback on_init_callback;
    OnDataCallback on_data_callback;
    OnConnectCallback on_connect_callback;
    OnDisconnectCallback on_disconnect_callback;
    Properties properties;
  };


  itk_felix_client(Config config);

  ~itk_felix_client();

  void send_data(uint64_t fid, const uint8_t* data, size_t size, bool flush);
  
  void send_data(uint64_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes);

  void subscribe(const std::vector<uint64_t>& fids);

  void subscribe(uint64_t fid);

  void unsubscribe(uint64_t fid);

  void exec(const UserFunction &user_function);

  Status send_cmd(const std::vector<uint64_t>& fids, Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<Reply>& replies);

 private:

  OnInitCallback cb_on_init;
  OnDataCallback cb_on_data = [](auto&&...) {};
  OnConnectCallback cb_on_connect;
  OnDisconnectCallback cb_on_disconnect;

  struct netio_context ctx;
  struct netio_timer subscribe_timer;
  struct netio_timer send_connection_timer;
  struct netio_timer user_timer;

  std::thread thread;

  
};

#endif
