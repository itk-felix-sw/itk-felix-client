#ifndef ITK_FELIX_BUS_INFO
#define ITK_FELIX_BUS_INFO

#include "nlohmann/json.hpp"
#include <string>
#include <cstdint>

class itk_felix_bus_info{

 public:
  
  itk_felix_bus_info();

  itk_felix_bus_info(const itk_felix_bus_info& info);

  ~itk_felix_bus_info();

  nlohmann::json serialize();
  
  void deserialize(nlohmann::json info);
  
  bool operator==(const itk_felix_bus_info& info);
  
  bool operator!=(const itk_felix_bus_info& info);

  std::ostream& operator<< (std::ostream &os);
    
 public:
 
  uint64_t fid;
  std::string hfid;
  std::string host;
  pid_t pid;
  std::string user;
  std::string ip;
  uint32_t port;
  bool unbuffered;
  bool pubsub;
  bool raw_tcp;
  bool stream;
  uint32_t netio_pages;
  uint64_t netio_pagesize;

 private:
  
  
};

std::ostream& operator<< (std::ostream &os, const itk_felix_bus_info& info);

#endif
