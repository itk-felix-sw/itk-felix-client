#ifndef ITK_FELIX_CONFIG_H
#define ITK_FELIX_CONFIG_H

class itk_felix_config{
  
 public:

  /**
   * @brief Callback executed by event loop thread at start.
   */
  std::function<void ()> OnInitCallback;
  
  /**
   * @brief Callback by event loop thread blocking the reception of new data
   */
  std::function<void (uint64_t fid, const uint8_t* data, size_t size, uint8_t status)> OnDataCallback;
  
  /**
   * @brief Callback reporting that a connection has been established.
   * @param fid The fid for which the connection is up.
   */
  std::function<void (uint64_t fid)> OnConnectCallback;

  /**
   * @brief Callback reporting that a connection has been closed.
   * @param fid The fid for which the connection is down.
   */ 
  std::function<void (uint64_t fid)> OnDisconnectCallback;
  
  /**
   * @brief User function to be passed to exec() for execution in the event loop.
   */    
  std::function<void ()> UserFunction;

  /**
   * Data structure to hold configuration parameters 
   */ 
  std::map<std::string, std::string> Properties;

};

#endif 
