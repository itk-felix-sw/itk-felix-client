#include "itk-felix-client/itk-felix-client.h"


itk_felix_client::itk_felix_client(Config config){

  if (config.on_init_callback) { cb_on_init=config.on_init_callback;}
  if (config.on_data_callback) { cb_on_data=config.on_data_callback;}
  if (config.on_connect_callback) { cb_on_connect=config.on_connect_callback;}
  if (config.on_disconnect_callback) { cb_on_disconnect=config.on_disconnect_callback;}

  netio_init();

  m_bus.set_path(config.property["bus_dir"]);
  m_bus.set_group(config.property["bus_group_name"]);
  
}

itk_felix_client::~itk_felix_client(){
  
}

void itk_felix_client::send_data(uint64_t fid, const uint8_t* data, size_t size, bool flush){
  //check socket type
  itk_felix_bus_info info = m_bus.get_info(fid);
  if(info.unbuffered){
    //send unbuffered
  }else{
    //send buffered
  }

}
  
void itk_felix_client::send_data(uint64_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes){
  //not implemented
}

void itk_felix_client::subscribe(const std::vector<uint64_t>& fids);

void itk_felix_client::subscribe(uint64_t fid);

void itk_felix_client::unsubscribe(uint64_t fid);

void itk_felix_client::exec(const UserFunction &user_function);

Status itk_felix_client::send_cmd(const std::vector<uint64_t>& fids, Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<Reply>& replies);
