#include "itk-felix-client/itk-felix-bus-info.h"

using namespace std;

itk_felix_bus_info::itk_felix_bus_info(){}

itk_felix_bus_info::itk_felix_bus_info(const itk_felix_bus_info& info){
  fid=info.fid;
  hfid=info.hfid;
  host=info.host;
  ip=info.ip;
  netio_pages=info.netio_pages;
  netio_pagesize=info.netio_pagesize;
  pid=info.pid;
  port=info.port;
  pubsub=info.pubsub;
  raw_tcp=info.raw_tcp;
  unbuffered=info.unbuffered;
  user=info.user;
}

itk_felix_bus_info::~itk_felix_bus_info(){}

nlohmann::json itk_felix_bus_info::serialize(){
  nlohmann::json info;
  info["fid"]=fid;
  info["hfid"]=hfid;
  info["host"]=host;
  info["ip"]=ip;
  info["netio_pages"]=netio_pages;  
  info["netio_pagesize"]=netio_pagesize;  
  info["pid"]=pid;
  info["port"]=port;
  info["pubsub"]=pubsub;
  info["raw_tcp"]=raw_tcp;
  info["unbuffered"]=unbuffered;
  info["user"]=user;
  return info;
}

void itk_felix_bus_info::deserialize(nlohmann::json info){
  fid=info["fid"];
  hfid=info["hfid"];
  host=info["host"];
  ip=info["ip"];
  netio_pages=info["netio_pages"];  
  netio_pagesize=info["netio_pagesize"];  
  pid=info["pid"];
  port=info["port"];
  pubsub=info["pubsub"];
  raw_tcp=info["raw_tcp"];
  unbuffered=info["unbuffered"];
  user=info["user"];
}

bool itk_felix_bus_info::operator==(const itk_felix_bus_info& info){
  if (host != info.host) return false;
  if (pid != info.pid) return false;
  if (user != info.user) return false;
  if (fid != info.fid) return false;
  if (hfid != info.hfid) return false;
  if (ip != info.ip) return false;
  if (port != info.port) return false;
  if (unbuffered != info.unbuffered) return false;
  if (pubsub != info.pubsub) return false;
  if (raw_tcp != info.raw_tcp) return false;
  if (stream != info.stream) return false;
  if (netio_pages != info.netio_pages) return false;
  if (netio_pagesize != info.netio_pagesize) return false;
  return true;
}
  
bool itk_felix_bus_info::operator!=(const itk_felix_bus_info& info){
  return !(*this==info);
}

std::ostream& itk_felix_bus_info::operator<< (std::ostream &os){
  os << "fid             " << fid << endl;
  os << "hfid            " << hfid << endl;
  os << "host:           " << host << endl;
  os << "ip:             " << ip << endl;
  os << "port:           " << port << endl;
  os << "pid:            " << pid << endl;
  os << "unbuffered:     " << unbuffered << endl;
  os << "pubsub:         " << pubsub << endl;
  os << "raw_tcp:        " << raw_tcp << endl;
  os << "user:           " << user << endl;
  os << "netio_pages:    " << netio_pages << endl;
  os << "netio_pagesize: " << netio_pagesize << endl;
  return os;
}

std::ostream& operator<< (std::ostream &os, const itk_felix_bus_info& info){
  os << "fid             " << info.fid << endl;
  os << "hfid            " << info.hfid << endl;
  os << "host:           " << info.host << endl;
  os << "ip:             " << info.ip << endl;
  os << "port:           " << info.port << endl;
  os << "pid:            " << info.pid << endl;
  os << "unbuffered:     " << info.unbuffered << endl;
  os << "pubsub:         " << info.pubsub << endl;
  os << "raw_tcp:        " << info.raw_tcp << endl;
  os << "user:           " << info.user << endl;
  os << "netio_pages:    " << info.netio_pages << endl;
  os << "netio_pagesize: " << info.netio_pagesize << endl;
  return os;
}
