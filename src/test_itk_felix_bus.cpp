#include "itk-felix-client/itk-felix-bus.h"
#include <cmdl/cmdargs.h>
#include <iostream>
#include <map>

using namespace std;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# test_itk_felix_bus            #" << endl
       << "#################################" << endl;
  
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cBus(    'b',"bus", "path","felix bus directory");
  CmdArgStr  cGroup(  'g',"group","group","felix group");

  cBus="/tmp/bus";
  cGroup="FELIX";

  CmdLine cmdl(*argv,&cVerbose,&cBus,&cGroup,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  itk_felix_bus bus;
  bus.set_verbose(cVerbose);
  bus.set_path(string(cBus));
  bus.set_group(string(cGroup));
  bus.scan();
  for (itk_felix_bus_info info: bus.get_infos()){
    cout << info << endl;
  }
  
  cout << "Have a nice day" << endl;
}
