#include "itk-felix-client/itk-felix-bus.h"
#include <sstream>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <filesystem>
#include "nlohmann/json.hpp"

using namespace std;

itk_felix_bus::itk_felix_bus(){
  m_path="./bus";
  m_group="FELIX";
  m_verbose=false;
}

itk_felix_bus::~itk_felix_bus(){}

void itk_felix_bus::set_path(const string& path) {
  m_path = path;
}

void itk_felix_bus::set_group(const string& group) {
  m_group = group;
}

void itk_felix_bus::set_verbose(bool verbose) {
  m_verbose = verbose;
}

void itk_felix_bus::set_info(uint64_t fid, string filename, itk_felix_bus_info &info){

}

itk_felix_bus_info itk_felix_bus::get_info(uint64_t fid){
  
  uint8_t did = (fid >> 52) & 0xFF;
  uint32_t cid = (fid >> 36) & 0xFFFF;
  
  stringstream fp;
  fp << m_path << "/" << m_group << "/";
  fp << setfill('0') << setw(1) << hex << did;
  fp << "/";
  fp << setfill('0') << setw(1) << hex << cid;

  scan_dir(fp.str());

  itk_felix_bus_info info;
  if(m_infos.count(fid)) return info;
  return m_infos[fid];
}

void itk_felix_bus::scan(){
  stringstream bp;
  bp << m_path << "/" << m_group;
  for(const filesystem::directory_entry& de : filesystem::recursive_directory_iterator(bp.str())){
    if(m_verbose) cout << "Trying path: " << de.path().string() << endl;
    if(de.path().string().find(".ndjson")==string::npos){continue;}
    load_file(de.path().string());
  }
}  

void itk_felix_bus::scan_dir(const string& path){
  for(const filesystem::directory_entry& de : filesystem::directory_iterator{path}){
    if(de.path().string().find(".ndjson")==string::npos){continue;}
    load_file(de.path().string());
  }
}

void itk_felix_bus::load_file(const string& path){
  ifstream fr;
  string line;
  fr.open(path);
  if(fr.is_open()){
    if(m_verbose) cout << "Loading file: " << path << endl;
    while(fr >> line){
      nlohmann::json entry = nlohmann::json::parse(line);
      if(m_verbose) cout << entry << endl;
      itk_felix_bus_info info;
      info.deserialize(entry);
      if(m_verbose) cout << "info: " << info << endl;
      m_infos[info.fid]=info;
    }
  }else{
    if(m_verbose) cout << "Could not load file: " << path << endl; 
  }
}

vector<itk_felix_bus_info> itk_felix_bus::get_infos(){
  vector<itk_felix_bus_info> vec;
  for(auto it: m_infos){
    vec.push_back(it.second);
  }
  return vec;
}
